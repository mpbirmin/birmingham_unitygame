﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMover : MonoBehaviour {

	public float movementSpeed;
	public int invert = -1;
	public float tilt;
	public Boundary boundary;
	public GameObject shotspawn;
	public GameObject yWingBox;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Rigidbody rb = GetComponent<Rigidbody> ();
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");
		Vector3 direction = new Vector3 (horizontal, invert * vertical, 0);
		Vector3 finalDirection = new Vector3 (horizontal, invert * vertical, 12.0f);
		transform.position += direction*movementSpeed*Time.deltaTime;
		transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (finalDirection), Mathf.Deg2Rad * 30.0f);
		rb.position = new Vector3
			(
				Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax), 
				Mathf.Clamp (GetComponent<Rigidbody>().position.y, -4, 4),
				0.0f
			);
		yWingBox.transform.localRotation = Quaternion.Euler (0.0f, 0.0f, (horizontal*-tilt));
	}
}
