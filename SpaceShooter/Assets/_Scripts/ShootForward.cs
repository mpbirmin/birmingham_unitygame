﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootForward : MonoBehaviour {
	public GameObject bullet;
	public GameObject ionShot;
	public float ionTimer;
	public AudioSource ionSound;
	public Transform shotspawn;
	public Transform shotspawn2;


	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1")){
			Instantiate(bullet, shotspawn.position, shotspawn.rotation);
			Instantiate(bullet, shotspawn2.position, shotspawn2.rotation);
			GetComponent<AudioSource>().Play ();
		}
		if (Input.GetButton("Fire2")){
			ionTimer += ionTimer * Time.deltaTime;
			if (ionTimer > 3) {
				ionTimer = 3;
			}
		}
		if (Input.GetButtonUp ("Fire2") && ionTimer == 3) {
			Instantiate (ionShot, shotspawn.position, shotspawn.rotation);
			ionSound.Play ();
			ionTimer = 1;
		}
		else if (Input.GetButtonUp ("Fire2") && ionTimer != 3) {
			ionTimer = 1;
		}
	}
}
