﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceMove : MonoBehaviour {
	public float speed;
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * speed * Time.deltaTime;
	}
		void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Ion")) {
			speed = 0.0f;
		}
	}
}
