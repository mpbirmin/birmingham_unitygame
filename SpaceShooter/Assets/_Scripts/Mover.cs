﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	public float speed;
	public Transform shotspawn;
	void Start ()
	{
		Rigidbody rb = GetComponent<Rigidbody> ();
		rb.rotation = shotspawn.rotation;
		rb.velocity = transform.forward * speed;
		print (rb.velocity);
		}
}
